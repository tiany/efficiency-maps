import sys

sys.dont_write_bytecode = True

import ROOT
from binning_functions import *
from helper_functions import *
from get_hist import *
import os
import json
from ROOT import gStyle
from labels import *
from plotting_tools import *

#Get bin type and name of file for 2D maps
name = sys.argv[1]
type = sys.argv[2]

if type == 'f':
    dir_type = 'finely_binned'
    binning = 'finely'
elif type == 'c':
    dir_type = 'calibration_binned'
    binning = 'calibrated'
else:
    dir_type = 'calibration_binned'
    binning = 'calibrated'
k=0
maps = {}
rent = []

ROOT.gROOT.SetBatch(1)
#Open file location
file = dir_type+'/'+name+'eta_vs_pt.root'
filec = dir_type+'/'+name+'eta_vs_pt_cont.root'
f = ROOT.TFile.Open(file)
fc = ROOT.TFile.Open(filec)
ROOT.gErrorIgnoreLevel = ROOT.kWarning
h, histnames = readdir(maps,f,k,rent) #Create dictionary of all the maps
hc, histnamesc = readdir(maps,fc,k,rent) #Create dictionary of all the maps

print len(h)+len(hc)
print '-----------------------------'
#Create respective directories for binned maps
makedir('vr_maps_2D')
makedir('vr_maps_2D',dir_type)
dir = 'vr_maps_2D/'+dir_type

jetCols,taggers,workingPoints,flavours,sample_names,twbin_list = labels() #Get the names of labels
b=0
# c=0

for tagger,jetcol,wp,flav,sample,tw in product(taggers,jetCols,workingPoints,flavours,sample_names,twbin_list):
    if wp == 'Continuous':
        # continue
        for tw in twbin_list[tagger]:
            c = ROOT.TCanvas('c','c',1000,750)
            pathname = '/'.join([binning,tagger,jetcol,wp,flav,tw,sample]) #Dictionary key name
            plotname = '_'.join([binning,tagger,jetcol,wp,flav,tw,sample])
            # print pathname
            hc[pathname].Draw('col Z')
            saver(c,plotname,dir,tagger,jetcol,wp,tw,flav)
            b=b+1
            if b % 30 == 0:
                print b #Print out the number of maps being saved
            del c
    else:
        c = ROOT.TCanvas('c','c',1000,750)
        pathname = '/'.join([binning,tagger,jetcol,wp,flav,sample]) #Dictionary key name
        plotname = '_'.join([binning,tagger,jetcol,wp,flav,sample]) #Name of plot
        h[pathname].Draw('col Z') #Draw the histogram in colour
        saver(c,plotname,dir,tagger,jetcol,wp,flav,sample) #Save the 2D map in pdf format
        b=b+1
        if b % 30 == 0:
            print b #Print out the number of maps being saved
        del c
    del pathname
    del plotname
