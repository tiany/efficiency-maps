import sys
import ROOT
from itertools import product
from binning_functions import *
from helper_functions import *
from get_hist import *
from labels import *
from plotting_tools import *
import os


def main():
    k=0
    maps = {}
    rent = []
    prefix = sys.argv[1]
    #Select bin type
    if sys.argv[2] == 'f':
        bin_type = 'finely'
        dir_type = 'finely_binned'
    elif sys.argv[2] == 'c':
        bin_type = 'calibrated'
        dir_type = 'calibration_binned'
    else:
        bin_type = 'calibrated'
        dir_type = 'calibration_binned'
    #Create directories for efficiency maps
    makedir('vr_eff_maps')
    new_dir_type = makedir('vr_eff_maps',dir_type)
    makedir(new_dir_type,'eff_vs_pt')
    makedir(new_dir_type,'eff_vs_eta')
    makedir(new_dir_type,'ratio_pt')
    makedir(new_dir_type,'ratio_eta')
    #Select eta or pt directory to make maps
    eta_directory = dir_type+'/'+prefix+'eff_vs_eta.root'
    pt_directory = dir_type+'/'+prefix+'eff_vs_pt.root'
    #Open the directories
    feta = ROOT.TFile.Open(eta_directory)
    fpt = ROOT.TFile.Open(pt_directory)
    ROOT.gROOT.SetBatch(1) #Run in batch mode
    ROOT.gErrorIgnoreLevel = ROOT.kWarning #Don't output unnecessary error notifications
    #Create dictionaries for eta and pt containing all the histograms
    heta,names_eta = readdir(maps,feta,k,rent)
    hpt,names_pt = readdir(maps,fpt,k,rent)
    #Print out total amount of histograms
    print len(heta)+len(hpt)
    print '-----------------------------'
    #Colours for generators
    colours = [
    ROOT.kRed,      #PhPy8EG
    ROOT.kBlue,     #Sherpa221
    ROOT.kGreen+1,  #PowhegHerwig7
    ROOT.kPink+9,   #PowhegHerwig713
    ROOT.kOrange+1, #aMcAtNloPy8EvtGen
    ROOT.kTeal-2,   #AtNloHerwig7
    ROOT.kMagenta+2 #Sh_N30NNLO
    ]
    #Define directory names
    pt_local = 'vr_eff_maps/'+dir_type+'/eff_vs_pt'
    eta_local = 'vr_eff_maps/'+dir_type+'/eff_vs_eta'
    ratio_pt = 'vr_eff_maps/'+dir_type+'/ratio_pt'
    ratio_eta = 'vr_eff_maps/'+dir_type+'/ratio_eta'
    #Get the current collections, taggers, workingpoints and taggers
    jetCols,taggers,workingPoints,flavours,sample_names,twbin_list = labels()
    #Create Plot Title
    title1 = 'Efficiency vs Pt'
    title2 = 'Efficiency vs Eta'
    b=0
    c=0

    for flav,jetcol,wp,tagger in product(flavours,jetCols,workingPoints,taggers):

        ptbinning,etabinning = binning(flav,bin_type) #Choose the binnings for pt and eta

        l_pt=len(ptbinning) #binning for eta map
        l_eta=len(etabinning) #binning for pt map

        # if b >= 500:
        #     print 'Ending the plotting...'
        #     break

        #Make Eff vs Pt Plots
        for i in range(l_eta-1):
            binx='eta={'+str(etabinning[i])+'-'+str(etabinning[i+1])+'}' #Select bins
            if wp=='Continuous':
                twbins = twbin_list[tagger] #Choose tag weight bins corresponding to taggers
                for twbin in twbins:
                    info = [bin_type,tagger,jetcol,wp,flav,binx,twbin] #Information for maps
                    pathname = '/'.join([bin_type,tagger,jetcol,wp,flav,binx,twbin]) #Dictionary key name
                    create_map(hpt,pathname,colours,sample_names,title1,info,pt_local) #Create Efficiency Map
                    create_ratio(hpt,pathname,colours,sample_names,title1,info,ratio_pt) #Create Efficiency Ratio
                    b=b+1
                    if b % 50 == 0:
                        print b
            else:
                info = [bin_type,tagger,jetcol,wp,flav,binx] #Information for maps
                pathname = '/'.join([bin_type,tagger,jetcol,wp,flav,binx]) #Dictionary key name
                create_map(hpt,pathname,colours,sample_names,title1,info,pt_local) #Create Efficiency Map
                create_ratio(hpt,pathname,colours,sample_names,title1,info,ratio_pt) #Create Efficiency Ratio

                b=b+1
                if b % 50 == 0:
                    print b

        #Makre Eff vs Eta Plots
        for j in range(l_pt-1):
            biny='pt={'+str(ptbinning[j])+'-'+str(ptbinning[j+1])+'}'
            if wp=='Continuous':
                for twbin in twbins:
                    info = [bin_type,tagger,jetcol,wp,flav,binx,twbin] #Information for maps
                    pathname = '/'.join([bin_type,tagger,jetcol,wp,flav,biny,twbin]) #Dictionary key name
                    create_map(heta,pathname,colours,sample_names,title2,info,eta_local) #Create Efficiency Map
                    create_ratio(heta,pathname,colours,sample_names,title2,info,ratio_eta) #Create Efficiency Ratio
                    b=b+1
                    if b % 50 == 0:
                        print b
            else:
                info = [bin_type,tagger,jetcol,wp,flav,biny] #Information for maps
                pathname = '/'.join([bin_type,tagger,jetcol,wp,flav,biny]) #Dictionary key name
                create_map(heta,pathname,colours,sample_names,title2,info,eta_local) #Create Efficiency Map
                create_ratio(heta,pathname,colours,sample_names,title2,info,ratio_eta) #Create Efficiency Ratio

                b=b+1
                if b % 50 == 0:
                    print b

    feta.Close()
    fpt.Close()
    print b
main()
