import sys
import ROOT
from itertools import product
from binning_functions import *
from helper_functions import *
from get_hist import *
from labels import *
import os
import json
from ROOT import gStyle
import glob
from calibration_bins import CalibrationBinning
from continuous_bins import continuous_bins


mapsversion = sys.argv[1] #Name of config file
outputfile_prefix = sys.argv[2] #Prefix of efficiency maps
bin_type = sys.argv[3] #Choose either 'f' or 'c' for finely_binned or calibration_binned
#Create full config file name
configfilename = 'maps_config_ATofficial_'+mapsversion+'.json'

print ''
print '-------------------------'
print '---------PART 1----------'
print '    Create config file   '
print '-------------------------'
print ''
#Make config file
os.system('python create_inputs_AT.py '+configfilename)

print ''
print '-------------------------'
print '---------PART 2----------'
print '    Create histogram root files   '
print '-------------------------'
print ''
#Create root files containing the histograms for efficiency maps
os.system('python create_eff.py '+configfilename+' '+outputfile_prefix+bin_type)

print ''
print '-------------------------'
print '---------PART 3----------'
print '    Create stacked efficiency maps in directories   '
print '-------------------------'
print ''
#Create stacked efficiency maps and Scale Factors in terms of samples
os.system('python comb_eff.py '+outputfile_prefix+' '+bin_type)

print ''
print '-------------------------'
print '---------PART 4----------'
print '  Create 2D Histograms   '
print '-------------------------'
print ''
#Create 2D efficiency maps in directories
os.system('python 2d_maps.py '+outputfile_prefix+' '+bin_type)
