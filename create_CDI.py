import json
import sys
import ROOT
ROOT.xAOD.Init().ignore()

from ROOT import *
from labels import *

inputfilename = sys.argv[1]
configfile = sys.argv[2]
outputfile_name = sys.argv[3]

print "os.path.exists(inputfilename)",os.path.exists(inputfilename)
if not os.path.exists(inputfilename):
	#fin = 'hadd %s calibrated/%s finely/%s'%(inputfilename,inputfilename,inputfilename)
	fin = 'hadd %s calibration_binned/%s finely_binned/%s'%(inputfilename,inputfilename,inputfilename)
	os.system(fin)
inputfile = ROOT.TFile(inputfilename,'read')
map_configs = json.load(open(configfile,'r'))

listofmaps = [x for x in map_configs.keys()]

binnings = ['calibrated','finely']

for map_i in range(len(listofmaps)):

	map_name = listofmaps[map_i]
	map_name = str(map_name)

	config = map_configs[map_name]

	tagger 		 	= str(config['tagger'])
	wp 			 	= str(config['wp'])
	flav 		 	= str(config['flav'])
	jetcol 		 	= str(config['jetcol'])
	samplelist 	 	= [str(x) for x in config['sample_names']]
	nominal_sample 	= samplelist[0]

	flav_name = flav
	if flav_name=='L':
		flav_name='Light'


	for binning in binnings:
		if binning == 'calibrated':
			end = '_CalibrationBinned_Eff'
			link = 'calibration_binned'
			isRef = True
		else:
			end = '_Eff'
			link = 'finely_binned'
			isRef = False
		for sample_i, sample_name in enumerate(samplelist):
			print '%s_%s_%s_%s_%s'%(tagger,wp,flav,sample_name,binning)
			outfile = ROOT.TFile(outputfile_name,'update')

			if sample_name == 'PhPy8EG':
				defaultSample = True
				print 'Here'
			else:
				defaultSample = False

			sample_id = str(sample_ids[sample_name][0])
			hadron = str(sample_ids[sample_name][1])

			#Extract histogram from file and process into a CDI
			hname = ('hist_%s_%s_%s_%s_%s_Eff')%(tagger,wp,flav,sample_name,binning)
			print hname
			h = inputfile.Get(binning+'/'+tagger+'/'+jetcol+'/'+wp+'/'+flav+'/'+sample_name+'/'+hname)
			if not h:
				outfile.Close()
				continue
			if 'Con' in wp:
				h.GetXaxis().SetName("pt")
				h.GetYaxis().SetName("abseta")
				h.GetZaxis().SetName("tagweight")
			cnt = Analysis.CalibrationDataHistogramContainer(sample_id+end)
			cnt.setResult(h)
			cnt.setComment('efficiencies from sample '+sample_id)
			cnt.restrictToRange(True)
	 		if "Con" in wp: cnt.setInterpolated(False)
	 		else: cnt.setInterpolated(True)
			cnt.setHadronisation(hadron)

			print jetcol, flav, wp, tagger
			Analysis.addContainer(cnt, outfile, tagger, jetcol, wp, flav_name, sample_id+end, isRef)

			#if this is the default sample, create another copy of the eff map named default_Eff
			if defaultSample:
				cnt.setComment('default efficiencies from sample '+sample_id)
				Analysis.addContainer(cnt, outfile, tagger, jetcol, wp, flav_name, 'default'+end, isRef)


			outfile.Close()
