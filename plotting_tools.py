import sys
import ROOT
from itertools import product
from binning_functions import *
from helper_functions import *
from get_hist import *
from labels import *
import os

texlist = []
def draw_tex(flav,tagger,wp,bin,twbin=''):
    texlist.append( ROOT.TLatex(0.14,0.84,"ATLAS") )
    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(72);
    texlist[-1].SetLineWidth(2);
    texlist[-1].Draw('same');
    texlist.append( ROOT.TLatex(0.27,0.84,"Internal Simulation") )
    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(42);
    texlist[-1].SetLineWidth(2);
    texlist[-1].Draw("same");
    texlist.append( ROOT.TLatex(0.14,0.8,"#sqrt{s} = 13 TeV") )

    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(12);
    texlist[-1].SetLineWidth(1);
    texlist[-1].Draw('same');

    if wp=='Continuous':
        texlist.append( ROOT.TLatex(0.14,0.76,flav+'-jets '+tagger+'-'+wp+'-'+bin+'-'+twbin) )
    else:
        texlist.append( ROOT.TLatex(0.14,0.76,flav+'-jets '+tagger+'-'+wp+'-'+bin) )

    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(42);
    texlist[-1].SetTextSize(0.025);
    texlist[-1].SetLineWidth(1);
    texlist[-1].Draw('same');
    #print ts

def find_max(h_list):
    old_max = None
    old_min = None
    for h in h_list:
        max = h.GetBinContent(h.GetMaximumBin()) #Find max value in Y for sample
        #Set new max and min values for scaling of Y axis
        if old_max is None:
            old_max = max
        elif (old_max < max):
            old_max = max
    return max

def find_min(h_list):
    old_min = None
    for h in h_list:
        min = h.GetBinContent(h.GetMinimumBin()) #Find max value in Y for sample
        #Set new max and min values for scaling of Y axis
        if old_min is None:
            old_min = min
        elif (old_min > min):
            old_min = min
    return min

def map_format(h,sample_i,colours):
    h.SetLineColor(colours[sample_i]) #Set line colour based off generator
    h.SetMarkerColor(colours[sample_i]) #Set marker colour based off generator
    h.SetMarkerStyle(sample_i+1) #Set marker style based off generator
    h.SetFillColorAlpha(colours[sample_i],0.3) #Set Error bin fill colour

def readdir(MyArr,dir,lvl = 0, parents = None, name_list = []):

        if parents == None:
            parents = []
        if lvl > 0:
            parents.append(dir.GetName())
        keys = dir.GetListOfKeys()

        for key in keys:
            # print key.GetName()
            #print '\t',parents[:]
            if key.IsFolder():
                #print key.ReadObj()
                readdir(MyArr,key.ReadObj(),lvl+1,parents[:])
            else:
                name = '/'.join(parents)
                # print name
                MyArr[name]=dir.Get(key.GetName())
                name_list.append(name)
        return MyArr, name_list

def create_map(h,path_name,colours,sample_list,title,info,type):
    bin_type = info[0]
    tagger = info[1]
    jetcol = info[2]
    wp = info[3]
    flav = info[4]
    bin = info[5]
    if wp=='Continuous':
        twbin = info[6]
    c = ROOT.TCanvas('c','c',1000,750)
    c.SetGrid()
    if wp=='Continuous':
        hname = tagger+'_'+wp+'_'+flav+'_'+bin+'_'+twbin #Set title name
    else:
        hname = tagger+'_'+wp+'_'+flav+'_'+bin #Set title name
    #Find max and min of sample list
    path_list = [path_name + '/' + s for s in sample_list]
    h_list = []
    for sample_i,(sample) in enumerate(sample_list):
        path=path_name+'/'+sample
        h_list.append(h[path].Clone())
    # h_list = [h[p] for p in path_list]
    max = find_max(h_list)
    min = find_min(h_list)
    #Set up
    for sample_i,(sample) in enumerate(sample_list):
        path=path_name+'/'+sample
        h[path].SetLineWidth(2)
        #Format map based on sample
        # h.SetLineColor(colours[sample_i]) #Set line colour based off generator
        # h.SetMarkerColor(colours[sample_i]) #Set marker colour based off generator
        # h.SetMarkerStyle(sample_i+1) #Set marker style based off generator
        # h.SetFillColorAlpha(colours[sample_i],0.3) #Set Error bin fill colour
        map_format(h[path],sample_i,colours)
        #Set initial Draw and titles if not set previously
        if sample_i == 0:
            h[path].SetTitle(title+': '+hname)
            if flav == 'B':
                h[path].SetAxisRange(0.85*min,max*1.2,"Y")
                leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
            elif flav == 'T':
                h[path].SetAxisRange(0.65*min,max*1.5,"Y")
                leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
            elif flav == 'L':
                h[path].SetAxisRange(0.65*min,max*2.2,"Y")
                leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
            else:
                h[path].SetAxisRange(0.5*min,max*1.7,"Y")
                leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
            h[path].DrawCopy('PE2','')
            h[path].SetFillColor(0)
            h[path].DrawCopy('HIST SAME','')
        else:
            h[path].DrawCopy('PE2 SAME','') #Retains markers of each histogram
            h[path].SetFillColor(0)
            h[path].DrawCopy('HIST SAME','')
            leg.SetTextFont(32)
            leg.SetBorderSize(0)
        leg.AddEntry(h[path],sample,"lp")
    if wp=='Continuous':
        draw_tex(flav,tagger,wp,bin,twbin)
    else:
        draw_tex(flav,tagger,wp,bin)
    leg.Draw()
    if wp=='Continuous':
        saver(c,hname,type,tagger,jetcol,wp,flav,bin,twbin)
        # save_plot_cont(c,type,tagger,jetcol,wp,flav,bin,twbin,hname)
    else:
        saver(c,hname,type,tagger,jetcol,wp,flav,bin)
        # save_plot(c,type,tagger,jetcol,wp,flav,bin,hname)
    del c
    del leg
    del h_list
    del max
    del min

def create_ratio(h,path_name,colours,sample_list,title,info,type):
    bin_type = info[0]
    tagger = info[1]
    jetcol = info[2]
    wp = info[3]
    flav = info[4]
    bin = info[5]
    if wp=='Continuous':
        twbin = info[6]
    r = ROOT.TCanvas('r','r',1000,750)
    r.SetGrid()
    if wp=='Continuous':
        hname = tagger+'_'+wp+'_'+flav+'_'+bin+'_'+twbin #Set title name
    else:
        hname = tagger+'_'+wp+'_'+flav+'_'+bin #Set title name
    ratio_path = path_name+'/PhPy8EG' #Path for nominal generator
    #Initialize ratio plots and max and min values
    ratio = []
    #Find max and min of sample list
    path_list = [path_name + '/' + s for s in sample_list]
    # h_list = []
    for sample_i,(sample) in enumerate(sample_list):
        path=path_name+'/'+sample
        ratio.append(h[path].Clone('ratio'))
        ratio[sample_i].Divide(h[ratio_path]) #Take the ratio to nominal sample
        # ratio[sample_i].SetFillColor(0)
        ratio[sample_i].SetLineWidth(2)
        #Format map based on sample
        # h.SetLineColor(colours[sample_i]) #Set line colour based off generator
        # h.SetMarkerColor(colours[sample_i]) #Set marker colour based off generator
        # h.SetMarkerStyle(sample_i+1) #Set marker style based off generator
        # h.SetFillColorAlpha(colours[sample_i],0.3) #Set Error bin fill colour
        map_format(ratio[sample_i],sample_i,colours)
        ratio[sample_i].SetTitle(title+': '+hname)
        ratio[sample_i].GetYaxis().SetTitle('Ratio')
    max = find_max(ratio)
    min = find_min(ratio)

    for sample_i,(sample) in enumerate(sample_list): #Iterate over all the samples

        #Scale Y axis by max and min values
        #Set initial Draw and titles if not set previously
        if sample_i == 0:
            # ratio[sample_i].SetAxisRange(0.85*min,max*1.2,"Y")
            # ratio[sample_i].DrawCopy('HIST','')
            #Set Legend depending on Flavour
            if flav == 'B':
                ratio[sample_i].SetAxisRange(0.85*min,max*1.1,"Y")
                leg = ROOT.TLegend(0.65,0.67,0.9,0.9)
            elif flav == 'T':
                ratio[sample_i].SetAxisRange(0.65*min,max*1.4,"Y")
                leg = ROOT.TLegend(0.65,0.67,0.9,0.9)
            else:
                ratio[sample_i].SetAxisRange(0.65*min,max*1.3,"Y")
                leg = ROOT.TLegend(0.65,0.67,0.9,0.9)
            ratio[sample_i].DrawCopy('PE2','') #Draw Error bars
            ratio[sample_i].SetFillColor(0) #Don't fill up next histogram lines
            ratio[sample_i].DrawCopy('HIST SAME','') #Plots histogram with connected lines

            leg.SetTextFont(32)
            leg.SetBorderSize(0)
        else:
            if flav == 'B':
                ratio[sample_i].SetAxisRange(0.85*min,max*1.1,"Y")
            elif flav == 'T':
                ratio[sample_i].SetAxisRange(0.65*min,max*1.4,"Y")
            else:
                ratio[sample_i].SetAxisRange(0.65*min,max*1.3,"Y")
            ratio[sample_i].DrawCopy('PE2 SAME','') #Draw Error bars
            ratio[sample_i].SetFillColor(0) #Don't fill up next histogram lines
            ratio[sample_i].DrawCopy('HIST SAME','') #Plots histogram with connected lines
        leg.AddEntry(ratio[sample_i],sample,"lp") #Add legend entry
    if wp=='Continuous':
        draw_tex(flav,tagger,wp,bin,twbin)
    else:
        draw_tex(flav,tagger,wp,bin)
    leg.Draw()
    if wp=='Continuous':
        saver(r,hname,type,tagger,jetcol,wp,flav,bin,twbin)
        # save_plot_cont(r,type,tagger,jetcol,wp,flav,bin,twbin,hname)
    else:
        saver(r,hname,type,tagger,jetcol,wp,flav,bin)
        # save_plot(r,type,tagger,jetcol,wp,flav,bin,hname)
    del r
    del leg
    del ratio
    del max
    del min
