import itertools
import os
import sys


def labels():
    categories = ['maps_a','maps_d','maps_e']
    jetCols = ['AntiKt4EMPFlowJets_BTagging201903']
    taggers = ['DL1','DL1r','DL1rmu']
    workingPoints = ['FixedCutBEff_'+x for x in ['60','70','77','85']]+['Continuous']
    flavours = ['B','C','L','T']
    sample_names = ['PhPy8EG','Sherpa2210','Sherpa228']
    twbins = {
    #'DL1':['[-100-0.785000026226]','[0.785000026226-2.375]','[2.375-3.36500000954]','[3.36500000954-4.625]','[4.625-100]'],
    #'DL1r':['[-100-1.08500003815]','[1.08500003815-2.58500003815]','[2.58500003815-3.5150001049]','[3.5150001049-4.80499982834]','[4.80499982834-100]'],
    #'DL1rmu':['[-100-1.17499995232]','[1.17499995232-2.52500009537]','[2.52500009537-3.45499992371]','[3.45499992371-4.68499994278]','[4.68499994278-100]']
    'DL1':['[-100-0.365]','[0.365-2.015]','[2.015-3.095]','[3.095-4.415]','[4.415-100]'],
    'DL1r':['[-100-0.665]','[0.665-2.195]','[2.195-3.245]','[3.245-4.565]','[4.565-100]'],
    'DL1rmu':['[-100-0.785]','[0.785-2.195]','[2.195-3.155]','[3.155-4.445]','[4.445-100]']
    }
    return jetCols, taggers, workingPoints, flavours, sample_names, twbins

def binning(flav,bins = 'calibrated'):
    if bins == 'calibrated':
    	if flav == 'L':
    		#ptbinning,etabinning = [[10.0,30.0,60.0,150.0,300.0],[0,2.5]]
    		ptbinning,etabinning = [[20.0,50.0,100.0,150.0,300.0],[0.0,2.5]]
    	elif flav == 'B':
    		ptbinning,etabinning = [[20.0,30.0,40.0,60.0,85.0,110.0,140.0,175.0,250.0,400.0],[0,2.5]]
    		#ptbinning,etabinning = [[10.0,20.0,30.0,60.0,100.0,250.0],[0,2.5]]
    	elif flav == 'C':
    		ptbinning,etabinning = [[20.0,40.0,65.0,140.0,250.0],[0,2.5]]
    		#ptbinning,etabinning = [[10.0,20.0,40.0,65.0,140.0],[0,2.5]]
    	else:
    		ptbinning,etabinning = [[20.0,40.0,65.0,140.0,250.0],[0,2.5]]
    		#ptbinning,etabinning = [[10.0,20.0,40.0,65.0,140.0],[0,2.5]]
    elif bins == 'finely':
    	if flav == 'L':
    		ptbinning,etabinning = [20.0,50.0,100.0,150.0,300.0],[0, 0.6, 1.2, 1.8, 2.5]
    		#ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,200,400],[0.0,1,2.5]
    	elif flav == 'C':
    		ptbinning,etabinning = [20.0,40.0,65.0,140.0,250.0],[0, 0.6, 1.2, 1.8, 2.5]
    	elif flav == 'T':
    		ptbinning,etabinning = [20.0,40.0,65.0,140.0,250.0],[0, 0.6, 1.2, 1.8, 2.5]
    		#ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,240,400],[0.0,1,2.5]
    	elif flav == 'B':
    		ptbinning,etabinning = [20.0,30.0,40.0,60.0,85.0,110.0,140.0,175.0,250.0,400.0],[0, 0.6, 1.2, 1.8, 2.5]
    		#ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,200,260,320,400],[0.0,1,1.5,2.5] 
    return ptbinning,etabinning

def data_extract():
    datasets = {
    'PhPy8EG':'/eos/user/t/tiany/public/effMapSamples/merged_PowhegPythia8.root',
    'Sherpa2210':'/eos/user/t/tiany/public/effMapSamples/Sherpa2210.PFlow.output.root',
    'Sherpa228':'/eos/home-g/gfrattar/FTAG/Final/merged_Sherpa228.root'
    }
    return datasets
sample_ids = {
'PhPy8EG':['410470','Pythia8EvtGen'],
'Sherpa2210':['700122','Sherpa2210'],
'Sherpa228':['421152','Sherpa228']
}
