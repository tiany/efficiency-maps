import ROOT
from array import array
import math

def validation(hist,histname):
	nbinsx = hist.GetXaxis().GetNbins()
	nbinsy = hist.GetYaxis().GetNbins()
	for i in range(nbinsx):
		for j in range(nbinsy):
			if (hist.GetBinContent(i,j) <= 0) and (5 < i < 500) and (j < 50):
				print "Bins (x="+str(i)+",y="+str(j)+") in histogram "+str(histname)+" is empty!"

def validation_rebin(hist,histname):
	bin_list = []
	nbinsx = hist.GetXaxis().GetNbins()
	nbinsy = hist.GetYaxis().GetNbins()
	# print 'Number of bins '+str(nbinsx*nbinsy)
	for i in range(1,nbinsx+1):
		for j in range(1,nbinsy+1):
		# print 'Bin content '+str(hist.GetBinContent(i))
			if (hist.GetBinContent(i,j) <= 0):
				print "Bin ("+str(i)+"," +str(j)+") in histogram "+str(histname)+" is empty!"
				bin = '('+str(i)+','+str(j)+')'
				bin_list.append(bin)
	if len(bin_list) == 0:
		# continue
		print "No empty bins in "+str(histname)+"!"
	else:
		print str(len(bin_list)) + " empty bins: bins "
		print ' '.join(bin_list)
def validation_rebin_3D(hist,histname):
	bin_list = []
	nbinsx = hist.GetXaxis().GetNbins()
	nbinsy = hist.GetYaxis().GetNbins()
	nbinsz = hist.GetZaxis().GetNbins()
	# print 'Number of bins '+str(nbinsx*nbinsy)
	for i in range(1,nbinsx):
		for j in range(1,nbinsy):
			for k in range(1,nbinsz):

			# print 'Bin content '+str(hist.GetBinContent(i))
				if (hist.GetBinContent(i,j,k) <= 0):
					print "Bin ("+str(i)+"," +str(j)+","+str(k)+") in histogram "+str(histname)+" is empty!"
					bin = '('+str(i)+','+str(j)+','+str(k)+')'
					bin_list.append(bin)
	if len(bin_list) == 0:
		# continue
		print "No empty bins in "+str(histname)+"!"
	else:
		print str(len(bin_list)) + " empty bins: bins "
		print ' '.join(bin_list)

def load_hist_list(outputfile, hist_list):
	h_list = []
	for idx, (filename,histname) in enumerate(hist_list):
		h_list.append(load_hist(outputfile,filename,histname,idx))

	for idx in range(1,len(h_list)):
		h_list[0].Add(h_list[idx])
	return h_list[0]


def load_hist(outputfile,filename,histname,hist_index):
	infile = ROOT.TFile(filename,'read')
	outputfile.cd()
	print "histname: ",histname
	hist = infile.Get(str(histname)).Clone('h_'+str(hist_index))
	# validation(hist,histname)
	return hist

def get_hists(outputfile, config):

	wp 			 	= str(config['wp'])
	tagger 		 	= str(config['tagger'])
	flav 		 	= str(config['flav'])
	samplelist 	 	= [str(x) for x in config['sample_names']]
	sample_ids 	 	= [str(x) for x in config['sample_hadronisations']]

	total_hists = config['total_hists']
	passed_hists = config['passed_hists']

	total_histograms = {}
	pass_histograms = {}

	for sample_i, sample_name in enumerate(samplelist):

		total_histograms[sample_name] = []
		pass_histograms[sample_name] = []

		t_h = total_hists[sample_name]
		p_h = passed_hists[sample_name]

		for sublist in t_h:
			total_histograms[sample_name].append(load_hist_list(outputfile,sublist))
		for sublist in p_h:
			pass_histograms[sample_name].append(load_hist_list(outputfile,sublist))

		if wp=='Continuous':
			working_points = ['FixedCutBEff_'+x for x in ['85','77','70','60']]
			wp0 = working_points[0]
			t_h = []
			p_h = []
			for cont_bin in range(len(working_points)+1):
				t_h.append(total_histograms[sample_name][0])
				# print 'Total hit of tagweight bin '+str(cont_bin)+' of '+tagger+'_'+flav+'_'+sample_name+' '+str(total_histograms[sample_name][0].Integral())
				if cont_bin==0:
					passed_hist = total_histograms[sample_name][0].Clone('ph_'+str(cont_bin))
					passed_hist.Add(pass_histograms[sample_name][0],-1)
				elif cont_bin==len(working_points):
					passed_hist = pass_histograms[sample_name][-1]
				else:
					passed_hist = pass_histograms[sample_name][cont_bin-1].Clone('ph_'+str(cont_bin))
					passed_hist.Add(pass_histograms[sample_name][cont_bin],-1)
				# print 'Passed hit of tagweight bin '+str(cont_bin)+' of '+tagger+'_'+flav+'_'+sample_name+' '+str(passed_hist.Integral())
				p_h.append(passed_hist)
			total_histograms[sample_name] = t_h
			pass_histograms[sample_name] = p_h
		else:
			total_histograms[sample_name] = total_histograms[sample_name][0]
			pass_histograms[sample_name] = pass_histograms[sample_name][0]
			# if wp=='FixedCutBEff_60':
			# 	print type(total_histograms[sample_name][0])
			# 	print type(pass_histograms[sample_name][0])
			# 	print 'Total hit of FixedCutBEff_60_'+tagger+'_'+flav+'_'+sample_name+' '+str(total_histograms[sample_name].Integral())
			# 	print 'Passed hit of FixedCutBEff_60_'+tagger+'_'+flav+'_'+sample_name+' '+str(pass_histograms[sample_name].Integral())
	return total_histograms, pass_histograms
