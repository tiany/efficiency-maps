# Grid Config scripts
The following scripts produce the derivation histogram files and contain the config scripts necessary for VR trackjets. These are located in the directory 'grid_scripts'.

Sample_list_* contain the MC files that are of interest.

Config_* are the config files corresponding to VR trackjets.

In submit.py the following lines ought to be modified for submission:
- line 5 should be changed to include your username for submission to the Grid.
- line 7 select the appropriate CDIfile
- line 8 includes the Pile Up Reweighting root files if one is interested in including them. They are not included by default.
- line 9 should be changed for every submission as that indicates the version number
- line 45 can be uncommented to indicate the number of files that are to be run over.

To send the job to the grid, simply do the following:

```
cd grid_scripts/
source setup.sh

```

The jobs should be sent to the Grid for processing.


# Description of Efficiency Map Production Script
The following script produces root files containing histograms based of binning type (finely or calibrated binning), by dimension in terms of efficiency, eta and pt.

The script make_effmaps.py is an automatized script that will create a config file from derivations provided, extract information to produce histograms and store them in root files, as well as pdfs of efficiency maps stacked in terms of generators.

Each component of the efficiency map process can also be individually executed.

All explicit terms in terms of binning, flavours, taggers, jet collections are listed and can be changed in the labels.py.

## Fully Automatized Script
To execute make_effmaps.py, in your terminal type:

```

python make_effmaps.py <map_name> <prefix for output files> <bin_type= f or c>


```
(do not include the brackets <> for what you type, this applies for the rest of the commands listed in this README.md manual)

- map_name is whatever name you choose for the map config file version.
- prefix is the prefix for the output root files and folders where your root files and pdfs will be stored.
- bin_type is the type of binning you want to use. ‘f’ is for finely binned while ‘c’ is for calibration binned. Calibration binned is the default.

## Config File Script
The script create_inputs_AT.py creates the config file for the efficiency map production from derivations. Datasets to be used need to be manually added in the datasets dictionary, where the keys correspond with the generator. The histogram_dir points to the directory containing the datasets.

The jet collection (jetCols), flavour, taggers and workingpoints used are all defined in the labels.py header file. That is the only location where these are explicitly defined. Select or write in any tagging information you feel in there.

To run this script type in your terminal:

```

python create_inputs_AT.py <map_name>


```
the map_name is optional, as it will have a default name in case you don’t want to use your own.

## Efficiency Map Root Files
Second file that is executed is create_eff.py, this will produce the root files that the next two scripts will run over. The outputted root files are:

- 2D eta vs pt maps
- 1D efficiency vs pt maps
- 1D efficiency vs eta maps
- 3D eta vs pt continuous maps
- 1D efficiency vs tag weight (for fixed eta and pt)
- 1D total counts vs pt and eta
- 1D passed counts vs pt and eta

To run this script type in your terminal:

```

python create_eff.py <config_file> <prefix for output files> <bin_type= f or c>


```
config_file is the config file that was generated in the previous script.

## Stacked Efficiency Maps for Generators
The next script produces 1D efficiency maps stacked in terms of generators as pdfs.

To run this script type in your terminal:

```

python comb_eff.py <prefix> <bin_type= f or c>


```
The prefix and bin_type are the same as were described before.

The final script 2d_maps.py produces pdfs of the 2D eta vs pt maps that were produced by create_eff.py.

To run this script type in your terminal:

```

python 2d_maps.py <prefix> <bin_type= f or c>


```

## Validation script
Use the script validation.py in order to determine if histograms in root files contain any empty bins. The empty bin numbers of each histogram are outputted in the terminal.

To run this script type in your terminal:

```

python validation.py <root file path>


```
##Create CDI
Use the script create_CDI.py to build a CDI. If the calibration and finely binned files are not merged, the script will merge them with hadd. This merger can be done independently.

To run this script type in your terminal:

```

python create_CDI.py <input root file> <config file> <output file name>


```
## CDI Validation script
Use the validation_script.py to validate the smoothed CDI.

To run the script, include on line 52 the smooth CDI file path of the script. Then type in your terminal:

```

python validation_script.py


```
