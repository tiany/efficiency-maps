import os
import sys


#username = 'dstarko'
username = 'tiany'

CDIfile = 'xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root'
pufile_loc = '/afs/cern.ch/user/d/dstarko/public/efficiency_maps/' #pileup file location
version = 'v4' #Version number
samples = []
#Create list holding sample and config file names
for period in ['a','d','e']:
    for config in ['config_mc16'+period+'_PFlow.txt']:
        samplelist = 'sample_list_mc16'+period+'.txt'
        if 'VR' in config: #Choose variable radius config files
            suffix = '.VR.mc16'+period+'.'+version
        elif 'PFlow' in config:
            suffix = '.PFlow.mc16'+period+'.'+version
        pufile = pufile_loc+'All.NTUP_PILEUP.MC16'+period+'.root'
        samples.append([username,suffix,samplelist,config,pufile])


for sample in samples:
    ##########################################################################################################################
    suffix    = sample[1] #".pflow.v0"
    username  = sample[0] #os.getenv("USER")
    inputlist = sample[2] #"sample_list_beam_spot.txt"
    config    = sample[3] #'config_mc16e_Pflow.txt'
    #pufile = sample[4]
    ##########################################################################################################################
    ###########################################################################################################################
    dsList=open(inputlist,'r')
    lines=dsList.readlines()
    # Grid submission script
    def submitJob(ds,config) :

        com = "prun --useAthenaPackages "

        com += "--inDS " + ds + " "

        com += "--outputs=output.root "

        com += "--writeInputToTxt=IN:in.txt "

        com += "--exec=\"top-xaod "+config+" in.txt\" "
        print "com: ",com,"\n\n"
        com += "--extFile="+CDIfile+" "
        #com += "--nFiles=10 "
        oDS = "user."+username+"."+ds.replace("/","")+suffix

        #print oDS+"  has length: "+str(len(oDS))
        while len(oDS) > 115 :
            #print len(oDS)," too long!!!"
            splODS=oDS.split("_")
            splODS.pop(2)
            oDS="_".join(splODS)
            oDS.replace(' ','')

        #print "final: "+oDS+"  has length: "+str(len(oDS))

        com += "--outDS "+ oDS + " "

        com += " --mergeOutput "

        #com = oDS
        return com


    ############################################################################################################################################
    ############################################################################################################################################
    #Identify the mc files in the sample lists
    for ds in lines :
        if "#" in ds :
            continue
        if "mc" not in ds and "valid" not in ds and "group" not in ds and "data" not in ds:
            continue
        #print "///////////////////////////////////////////////////////////////////////////////////////"
        print ds
        command=submitJob(ds.replace("\n",""),config)
        print command
        os.system(command)
