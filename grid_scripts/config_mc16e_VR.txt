LibraryNames libTopEventSelectionTools libTopEventReconstructionTools

#NEvents 10000

### Good Run List
GRLDir  GoodRunsLists
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml

### Pile-up reweighting tool - Metadata determines which to use
# MC16a configuration
### Use following lines (uncomment) for your config file
#PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/prw.merged.root
#PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/prw.merged.root
#PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root #GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root

# MC16d configuration
#PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/CI.prw.merged.root
#PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.AF.v2/CI.prw.merged.root
#PRWActualMu_FS GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWActualMu_AF GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root

# MC16e configuration
#PRWConfigFiles All.NTUP_PILEUP.MC16e.root
#PRWActualMu_FS GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWActualMu_AF GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWLumiCalcFiles GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root

PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.FS.v2/prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.AF.v2/prw.merged.root
PRWActualMu_FS GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWActualMu_AF GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWLumiCalcFiles GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root

BTagCDIPath xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root

# showering algorithm
TDPPath XSection-MC16-13TeV.data

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMPFlowJets_BTagging201903
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None

TrackJetCollectionName AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903

TruthCollectionName None
TruthJetCollectionName None
TopPartonHistory False
TopParticleLevel False
TruthBlockInfo False
PDFInfo False

TruthElectronCollectionName None
TruthMuonCollectionName None
TruthJetCollectionName None

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

Systematics None
JetUncertainties_NPModel CategoryReduction
JetUncertainties_BunchSpacing 25ns

MCGeneratorWeights Nominal

EJetPt 5000
JetEta 2.5
TrackJetPt 5000
TrackJetEta 2.5
BTaggingWP DL1:FixedCutBEff_60 DL1:FixedCutBEff_70 DL1:FixedCutBEff_77 DL1:FixedCutBEff_85 DL1r:FixedCutBEff_60 DL1r:FixedCutBEff_70 DL1r:FixedCutBEff_77 DL1r:FixedCutBEff_85 DL1rmu:FixedCutBEff_60 DL1rmu:FixedCutBEff_70 DL1rmu:FixedCutBEff_77 DL1rmu:FixedCutBEff_85
#BTagVariableSaveList None


DoTight MC
DoLoose False

FilterBranches *

UseAodMetaData True
IsAFII False

RedefineMCMCMap sherpa228:pythia8,herwigpp713:pythia8,herwig:pythia8,sherpa21:pythia8,herwigpp:pythia8,sherpa:pythia8,amcatnlopythia8:pythia8

########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL
TRACKJETCLEAN

#########################
### FTag efficiency maps histograms
########################

SUB FTAG_EffPlots

#JETFTAGEFFPLOTS fill_total_hist WP:FixedCutBEff_60 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root dont_use_event_weight suffix:_no_event_weight

#JETFTAGEFFPLOTS fill_total_hist WP:FixedCutBEff_60 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:/afs/cern.ch/work/f/fdibello/public/TESTCDI-MC16-CDI-VR-20012020.root

JETFTAGEFFPLOTS fill_total_hist WP:FixedCutBEff_60 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root dont_use_event_weight suffix:_no_event_weight use_track_jets

JETFTAGEFFPLOTS fill_total_hist WP:FixedCutBEff_60 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1 N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1r  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1rmu  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets
#JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:MV2c10  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root use_track_jets


########################
### inclusive selection
########################
SUB ftag_basic
. BASIC

JETCLEAN LooseBad


SELECTION ftag
. ftag_basic
. FTAG_EffPlots
SAVE
