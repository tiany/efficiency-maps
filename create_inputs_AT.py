import os
import glob
from itertools import product
from calibration_bins import CalibrationBinning
from continuous_bins import continuous_bins
import json
import sys
from labels import *

wr=0
noweight = False

jetCols, taggers, workingPoints, flavours, sample_names, twbins = labels()
# sample_names = ['PhPy8EG','PowhegHerwig7','aMcAtNloPy8EvtGen','aMcAtNloHerwig7']
# hadronisations = ['Pythia8EventGen','Herwig7','Py8_aMCNLO','Herwig7_aMCNLO']

#histogram_dir = '/eos/home-d/dstarko/mc16_vr/'
histogram_dir = ''

if len(sys.argv[1])>0:
    configfilename = sys.argv[1]
else:
    mapsversion = 'for_mc16_VR_combined_samples_v2'

    if noweight:
      configfilename = 'maps_config_ATofficial_no_event_weight_'+mapsversion+'.json'
    else:
      configfilename = 'maps_config_ATofficial_'+mapsversion+'.json'

datasets = data_extract()

map_configs = {}

job_index=-1

for jetcol,tagger,wp,flav in product(jetCols,taggers,workingPoints,flavours):

  job_index+=1

  map_name = jetcol+'_'+tagger+'_'+wp+'_'+flav
  sample_names = list(datasets.keys())
  hadronisations = [item[1] for item in list(sample_ids.values())]

  map_config={}
  map_config['flav'] = flav
  map_config['jetcol'] = jetcol
  map_config['tagger'] = tagger
  map_config['wp'] = wp
  map_config['sample_names'] = sample_names
  map_config['sample_dsid'] = [item[0] for item in list(sample_ids.values())]
  map_config['sample_hadronisations'] = [item[1] for item in list(sample_ids.values())]

  total_hists = {}
  passed_hists = {}

  #for sample_name in input_samples:
  for sample_i, (sample_name,hadronisation) in enumerate(zip(sample_names,hadronisations)):
    #print sample_name
    if wp=='Continuous':
      infile = histogram_dir+datasets[sample_name]
      total_hists[sample_name] = [[],[],[],[]]
      passed_hists[sample_name] = [[],[],[],[]]
      for cont_bin,wp_i in enumerate(['FixedCutBEff_'+x for x in ['85','77','70','60']]):
          total_hist = 'ftag/JetFtagEffPlots/'+flav+'_total_'+jetcol
          pass_hist = 'ftag/JetFtagEffPlots/'+tagger+'_'+wp_i+'_'+jetcol+'_'+flav+'_pass'

          total_hists[sample_name][cont_bin].append([infile,total_hist])
          passed_hists[sample_name][cont_bin].append([infile,pass_hist])
      map_config['sample_ids'] = '410470'
      map_config['total_hists'] = total_hists
      map_config['passed_hists'] = passed_hists
      wr+=1
    else:
      total_hists[sample_name] = [[]]
      passed_hists[sample_name] = [[]]

        # for sample_dsid in ds_ids:
      infile = histogram_dir+datasets[sample_name]
      if noweight:
          total_hist = 'ftag/JetFtagEffPlots/'+flav+'_total_'+jetcol+'_no_event_weight'
          pass_hist = 'ftag/JetFtagEffPlots/'+tagger+'_'+wp+'_'+jetcol+'_'+flav+'_pass_no_event_weight'
      else:
          total_hist = 'ftag/JetFtagEffPlots/'+flav+'_total_'+jetcol
          pass_hist = 'ftag/JetFtagEffPlots/'+tagger+'_'+wp+'_'+jetcol+'_'+flav+'_pass'

      total_hists[sample_name][0].append([infile,total_hist])
      passed_hists[sample_name][0].append([infile,pass_hist])

      map_config['sample_ids'] = '410470'
      map_config['total_hists'] = total_hists
      map_config['passed_hists'] = passed_hists
      wr+=1
    map_configs[map_name] = map_config

  ##calib bins
  calib_bins_pt = []
  calib_bins_eta = []

  if jetcol+'_'+tagger+'_'+wp+'_'+flav in CalibrationBinning:
    calib_bins_pt, calib_bins_eta = CalibrationBinning[jetcol+'_'+tagger+'_'+wp+'_'+flav]


  map_config['CalibrationBinning'] = [calib_bins_pt, calib_bins_eta]
  if wp=='Continuous':
      map_config['tagweight_bins'] = continuous_bins['Continuous_'+jetcol+'_'+tagger]
  #end calib bins


config_file = open(configfilename,'w')
config_file.write(json.dumps(map_configs,indent=3,sort_keys=True))

print 'wrote ', wr, ' map configurations '
